# For singularity, we should avoid symlink folders
cd `pwd -P`
./nextflow main.nf --input_file output/segmentation.h5 \
                   --output_folder output_params
