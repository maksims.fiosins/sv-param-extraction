nextflow.enable.dsl=2

def processed_params = [:]
for (param in params){
  processed_params[param.key] = param.value
}

file_params = ["input_file", "output_folder"]
for (param in file_params){
  processed_params[param] = processed_params[param] ? file(processed_params[param]) : ""
}

default_string_params = []
for (param in default_string_params){
  processed_params[param] = (processed_params.containsKey(param) && (processed_params[param] != true)) ? processed_params[param] : ""
}

script = file("bin/extract_parameters.py")

process param_extraction {
    container 'registry.gitlab.com/maksims.fiosins/sv-param-extraction/sv-param-extraction:1.0.0'
    publishDir "${processed_params['output_folder']}", mode: 'copy', saveAs: { fn -> fn.substring(fn.lastIndexOf('/')+1) }

    input:
       val processed_params
       file script

    output:
       file "output_folder/*"


    """
     extract_parameters.py --input_file ${processed_params["input_file"]} \\
                   --output_folder "output_folder"

    """
}

workflow {
   param_extraction(processed_params, script)
}
